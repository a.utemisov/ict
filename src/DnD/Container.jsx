import update from 'immutability-helper'
import { useCallback, useContext, useEffect, useRef, useState } from 'react'
// import { Link } from 'react-router-dom'
import { Card } from './Card'
import { Links } from '../mockdata/SideBarLink'
import ToogleButton from '../Components/atoms/ToogleButton'
import { Link } from 'react-router-dom'
import { Edit } from '../Components/atoms/Edit'
import { GlobalContext } from '../GlobalContext'

export const Container = () => {
  const { setSelectedMenuKeys } = useContext(GlobalContext);
  const [cards, setCards] = useState(Links)

  const [toogle, setToogle] = useState(false)

  const moveCard = useCallback((dragIndex, hoverIndex) => {
    setCards((prevCards) =>
      update(prevCards, {
        $splice: [
          [dragIndex, 1],
          [hoverIndex, 0, prevCards[dragIndex]],
        ],
      }),
    )
  }, [])

  const renderCard = useCallback((card, index) => {
    return (
      <Card
        key={card.id}
        index={index}
        id={card.id}
        text={card.text}
        moveCard={moveCard}
        myMessage={card.myMessage}
      />
    )
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return (
    <>
      <div className='link-list-container w-[200px] text-center'>
        {/* <div>{height}</div> */}
        {toogle ?
          <div className='link-list'>{cards.map((card, i) => renderCard(card, i))}</div>
          :
          <div className='link-list'>{cards.map((card) => { return <div className='link-list-item-static border-solid border-[#052c5d]'><Link to={`/${card.url}`} onClick={() => setSelectedMenuKeys(card.text)} >{card.text}</Link></div> })}</div>
        }
        <ToogleButton className='w-full flex items-center justify-center px-2 py-2 bg-transparent border-[1px] border-[#052c5d] transition-all hover:bg-[#052c5d] hover:text-white' toogle={toogle} setToogle={setToogle}><Edit /></ToogleButton>
      </div>
    </>
  )
}


