import { createContext, useContext, useEffect, useState } from "react";

export const GlobalContext = createContext({
  selectedMenuKeys: [],
  setSelectedMenuKeys: () => {
    // init
  },
});

export const GlobalContextProvider = ({ children }) => {
  const [selectedMenuKeys, setSelectedMenuKeys] = useState('News');

  return (
    <GlobalContext.Provider
      value={{
        selectedMenuKeys,
        setSelectedMenuKeys,
      }}
    >
      {children}
    </GlobalContext.Provider>
  );
};

export const GlobalMeta = ({ selectedMenuKeys }) => {
  const pageMetaContext = useContext(GlobalContext);

  useEffect(() => {
    pageMetaContext.setSelectedMenuKeys(selectedMenuKeys);
  }, [selectedMenuKeys]);

  return <></>;
};
