const ToogleButton = ({setToogle, toogle, children, className}) => {

  const clickHandler = () => {
    setToogle((old) => !old)
  }

  return(
    <button className={`${className} ${toogle ? 'list-toogle-activate' : 'list-toogle-NONactivate'}`} onClick={clickHandler}>{children}</button>
  )
}

export default ToogleButton