
import { Button } from 'antd';
import { ReactComponent as EditIcon } from './Edit.svg';

export const Edit = () => {
  return <EditIcon />;
};
