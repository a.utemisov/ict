import { Layout } from "antd"
import { Outlet } from "react-router-dom"
import { AppFoter } from "../organisms/AppFoter"
import { AppHeader } from "../organisms/AppHeader"
import { SideBar } from "../organisms/SideBar"

const SharedLayout = () => {
  return (
    <Layout className='w-full h-screen flex flex-col justify-between '>
      <div>
        <AppHeader />
        <div className="w-full flex flex-row">
          <SideBar />
          <div className="flex flex-col border-l-2 w-full pt-5 px-5">
            <Outlet />
          </div>
        </div>
      </div>
      <AppFoter />
    </Layout>
  )
}
export default SharedLayout