import { DndProvider } from "react-dnd"
import { HTML5Backend } from "react-dnd-html5-backend"
import { Container } from "../../DnD/Container"
import { Logo } from "../atoms/Logo"

export const SideBar = () => {

  return (
    <div className="">
      <DndProvider backend={HTML5Backend}>
        <Container />
      </DndProvider>
    </div>)
}