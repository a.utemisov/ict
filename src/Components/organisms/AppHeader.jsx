
// import styled from '@emotion/styled';
import { Button, Dropdown, Menu, } from 'antd';
// import { Header as HeaderAntd } from 'antd/lib/layout/layout';
// import { Bell, Help, Logout, Menu, Messages } from '@components/atoms/Icon';
// import tw from 'twin.macro';
// import { LanguageSelector } from '@components/molecules/LanguageSelector';
// import { Avatar } from '@components/atoms/Avatar';
import { useContext, useState } from 'react';
import { GlobalContext } from '../../GlobalContext';
import { Logo } from '../atoms/Logo';

const langs = [
  {
    title: 'Ru',
    code: 'en',
  },
  {
    title: 'En',
    code: 'ru',
  },
  {
    title: 'Kz',
    code: 'kz',
  },
];
export const AppHeader = ({
  isSideBarCollapsed,
  setIsSideBarCollapsed,
}) => {
  const [selectedLang, setSelectedLang] = useState('Ru');
  const handleMenuItemClick = (l) => { setSelectedLang(l); }
  const { selectedMenuKeys } = useContext(GlobalContext)
  return (
    <div className='w-full h-[60px] flex flex-row justify-between pr-5 items-center border-b-2 bg-[rgb(20,52,90)]'>
      <div className=' flex flex-row items-center '>
        <div className='bg-white h-[60px]'>
          <Logo />
        </div>
        <div className='mx-7 text-white text-3xl'>
          {selectedMenuKeys}
        </div>
      </div>
      <div className='language-selector'>
        <Dropdown className='bg-white'
          overlay={
            <Menu>
              {langs.map((l) => (
                <Menu.Item onClick={() => handleMenuItemClick(l.code)} key={l.code}>
                  {l.title}
                </Menu.Item>
              ))}
            </Menu>
          }
          trigger={['click']}
        >
          <Button buttonSize="md" type="default">
            {selectedLang}
          </Button>
        </Dropdown>
      </div>
    </div>
  );
};
