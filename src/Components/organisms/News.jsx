import { Divider, List, Typography } from "antd"
import { Link } from "react-router-dom"
import { NewsData } from '../../mockdata/NewsData.js'
const News = () => {

  return (
    <section className="section">
      <List
        // header={
        //   // <div className="w-full flex flex-col items-center text-center "><h1 className="text-3xl">Новости системы</h1> </div>
        //   // <Divider className="my-3" />
        // }
        footer={<Divider />}
        className=''
        dataSource={NewsData}
        renderItem={item => (
          <List.Item className="h-10 flex flex-col m-0 p-0">
            <Typography.Text className={`w-full h-full flex flex-row justify-between text-center items-center px-5 ${item.isAlert && 'bg-[rgb(20,52,90)] text-white'}`}>
              <div className="flex flex-row items-center">
                {item.titile.length > 0 && <div className={`mr-5 ${item.isAlert ? 'text-white' : 'text-[#052c5d]'} text-lg`}>{item.titile}{' :'}</div>}
                {item.content}
              </div>
              <Link to={item.link} className={`${item.isAlert ? '!text-white' : '!text-[#052c5d]'}`}>подробнее...</Link>
            </Typography.Text>
            < Divider className="border-[#052c5d] my-0" />
          </List.Item>
        )
        }>
      </List >
    </section >
  )
}

export default News