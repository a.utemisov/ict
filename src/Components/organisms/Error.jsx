import { useContext } from "react"
import { GlobalContext } from '../../GlobalContext';
const Error = () => {
  const { selectedMenuKeys } = useContext(GlobalContext)
  return (
    <h1 className="error text-3xl">Тут будет {selectedMenuKeys}, а пока его нет</h1>
  )
}
export default Error