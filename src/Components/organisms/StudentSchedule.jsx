import { Select } from 'antd'
const StudentSchedule = () => {
  const day = 'day text-center bg-[#052c5d]'
  return (
    <div className=" flex flex-col items-center gap-2">
      <table className="w-full chars w-2/5 bg-white border-x-0 border-[rgb(11,33,59)] text-base" >
        <div className="flex flex-row p-5 ">
          <div className="flex flex-col text-right">
            <span >Student: </span>
            <span >ID: </span>
            <span >Spreciality: </span>
            <span >Study year: </span>
          </div>
          <div className="w-[1px] h-24 bg-black ml-5" ></div>
          <div className="flex flex-col ml-5">
            <span>Insar Istybaev</span>
            <span>21B00000</span>
            <span>Computing and software</span>
            <span>1</span>
          </div>
        </div>
      </table>
      <div className="custom-select " >
        <Select defaultValue='0'>
          <Select.Option value="0">2020-2021</Select.Option>
          <Select.Option value="2">2022-2023</Select.Option>
          <Select.Option value="1">2021-2022</Select.Option>
        </Select>
        <Select defaultValue='0'>
          <Select.Option value="0">autumn</Select.Option>
          <Select.Option value="1">spring</Select.Option>
        </Select>
      </div>
      <table className="timetable !text-xs">
        <tbody className="rows">
          <tr>
            <td className={day}><div className="thatDiv">days/time</div></td>
            <td className="day"><div className="thatDiv">mon</div></td>
            <td className="day"><div className="thatDiv">tue</div></td>
            <td className="day"><div className="thatDiv">wen</div></td>
            <td className="day"><div className="thatDiv">thu</div></td>
            <td className="day"><div className="thatDiv">fri</div></td>
            <td className="day"><div className="thatDiv">sat</div></td>
          </tr>
        </tbody>

        <tbody className="rows">
          <tr className="time">
            <td className={day}>8:00</td>
            <td className="subject"></td>
            <td className="subject"></td>
            <td className="subject">math</td>
            <td className="subject"></td>
            <td className="subject"></td>
            <td className="subject"></td>
          </tr>
        </tbody>
        <tbody className="rows">
          <tr className="time">
            <td className={day}>9:00</td>
            <td className="subject"></td>
            <td className="subject"></td>
            <td className="subject"></td>
            <td className="subject">biology</td>
            <td className="subject"></td>
            <td className="subject"></td>
          </tr>
        </tbody>
        <tbody className="rows">
          <tr className="time">
            <td className={day}>10:00</td>
            <td className="subject">physics</td>
            <td className="subject"></td>
            <td className="subject"></td>
            <td className="subject"></td>
            <td className="subject"></td>
            <td className="subject"></td>
          </tr>
        </tbody>
        <tbody className="rows">
          <tr className="time">
            <td className={day}>11:00</td>
            <td className="subject"></td>
            <td className="subject"></td>
            <td className="subject"></td>
            <td className="subject"></td>
            <td className="subject"></td>
            <td className="subject">Programing</td>
          </tr>
        </tbody>
        <tbody className="rows">
          <tr className="time">
            <td className={day}>12:00</td>
            <td className="subject"></td>
            <td className="subject">Art</td>
            <td className="subject"></td>
            <td className="subject"></td>
            <td className="subject"></td>
            <td className="subject"></td>
          </tr>
        </tbody>
        <tbody className="rows">
          <tr className="time">
            <td className={day}>13:00</td>
            <td className="subject"></td>
            <td className="subject"></td>
            <td className="subject"></td>
            <td className="subject"></td>
            <td className="subject">PE</td>
            <td className="subject"></td>
          </tr>
        </tbody>
        <tbody className="rows">
          <tr className="time">
            <td className={day}>14:00</td>
            <td className="subject">web</td>
            <td className="subject"></td>
            <td className="subject"></td>
            <td className="subject"></td>
            <td className="subject"></td>
            <td className="subject"></td>
          </tr>
        </tbody>
        <tbody className="rows">
          <tr className="time">
            <td className={day}>15:00</td>
            <td className="subject"></td>
            <td className="subject"></td>
            <td className="subject">philosophy</td>
            <td className="subject"></td>
            <td className="subject"></td>
            <td className="subject"></td>
          </tr>
        </tbody>
        <tbody className="rows">
          <tr className="time">
            <td className={day}>16:00</td>
            <td className="subject"></td>
            <td className="subject"></td>
            <td className="subject"></td>
            <td className="subject">english</td>
            <td className="subject"></td>
            <td className="subject"></td>
          </tr>
        </tbody>
        <tbody className="rows">
          <tr className="time">
            <td className={day}>17:00</td>
            <td className="subject">unkrainian</td>
            <td className="subject"></td>
            <td className="subject"></td>
            <td className="subject"></td>
            <td className="subject"></td>
            <td className="subject"></td>
          </tr>
        </tbody>
        <tbody className="rows">
          <tr className="time">
            <td className={day}>18:00</td>
            <td className="subject"></td>
            <td className="subject"></td>
            <td className="subject">kazakh</td>
            <td className="subject"></td>
            <td className="subject"></td>
            <td className="subject"></td>
          </tr>
        </tbody>
        <tbody className="rows">
          <tr className="time">
            <td className={day}>19:00</td>
            <td className="subject"></td>
            <td className="subject"></td>
            <td className="subject"></td>
            <td className="subject"></td>
            <td className="subject"></td>
            <td className="subject"></td>
          </tr>
        </tbody>
        <tbody className="rows">
          <tr className="time">
            <td className={day}>20:00</td>
            <td className="subject"></td>
            <td className="subject"></td>
            <td className="subject"></td>
            <td className="subject"></td>
            <td className="subject"></td>
            <td className="subject"></td>
          </tr>
        </tbody>
      </table>
    </div>
  )
}

export default StudentSchedule