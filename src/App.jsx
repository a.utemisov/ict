import { BrowserRouter, Route, Routes } from "react-router-dom";
import SharedLayout from "./Components/template/SharedLayout";
import './App.css'
import 'antd/dist/antd.css';
import Home from "./Components/Home";
import Error from "./Components/organisms/Error";
import StudentSchedule from "./Components/organisms/StudentSchedule";
import News from "./Components/organisms/News";
import { GlobalContextProvider } from "./GlobalContext";

function App() {
  return (
    <GlobalContextProvider>
      <div className="app-container">
        <BrowserRouter>
          <Routes>
            <Route path="/" element={<SharedLayout />}>
              <Route index element={<Home />} />
              <Route path="*" element={<Error />} />
              <Route path="/Students_journal" element={<StudentSchedule />} />
              <Route path="/News" element={<News />} />
            </Route>
          </Routes>
        </BrowserRouter>
      </div>
    </GlobalContextProvider>
  )
}

export default App;
